from datetime import datetime
import requests
import sys
import json


class WeatherForecast:

    def __init__(self, api_key, date=str(datetime.today().date())):
        self.api_key = api_key
        self.date = date
        self.data = self.get_data()
        self.weather = self.weather_info()

    def get_data(self):
        req_url = 'http://api.weatherapi.com/v1/history.json'
        payload = {'key': self.api_key, "q": "London", "dt": str(self.date)}
        r = requests.get(req_url, params=payload)
        content = r.json()
        return content


    def weather_info(self):
        initial_dict = {}
        with open('prognoz.json') as fp:
            # for line in fp.readlines():
            #     divided_line = line.split(',')
            #     date_ = divided_line[0]
            #     info_ = divided_line[1].replace('\n', '')
            #     initial_dict[date_] = {
            #         'info': info_,
            #     }
            initial_dict = json.load(fp)

            return initial_dict

    def get_rain_info(self):
        totalprecip_mm = float(self.data['forecast']['forecastday'][0]['day']['totalprecip_mm'])
        return self.get_rain_chance(totalprecip_mm)

    def get_rain_chance(self, totalprecip_mm):
        if totalprecip_mm > 0.0:
            return 'Będzie padać'
        elif totalprecip_mm == 0.0:
            return 'Nie będzie padać'
        return 'Nie wiem!'

    def get_dates(self):
        with open('prognoz.json', 'r') as file3:
            daty = []
            odczyt = json.load(file3)
            for key in odczyt.keys():
                daty.append(key)
            print(daty)

    def items(self):
        for date, value in self.weather.items():
            yield (date, value)

    def __getitem__(self, item):
        return self.weather.get(item, "Nie wiem")

    def __iter__(self):
        return iter(self.weather.keys())


if len(sys.argv) >= 3:
    wf = WeatherForecast(api_key=sys.argv[1], date=sys.argv[2])
else:
    wf = WeatherForecast(api_key=sys.argv[1])


with open('prognoz.json', 'r') as file:
    zapis = json.load(file)
    if str(sys.argv[2]) not in zapis.keys():
        zapis[sys.argv[2]] = wf.get_rain_info()

with open('prognoz.json', "w") as file2:
    json.dump(zapis, file2)


print(wf.get_rain_info().replace('\n', ''))
print(50 * "_")
for data, pogoda in wf.items():
    print(data, pogoda)
print(50 * "_")
print("Daty dla ktorych znana jest pogoda to:")
for v in wf:
    print(v)